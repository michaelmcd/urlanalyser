using AzureFunctions.Autofac.Configuration;
using Autofac;
namespace Octopus.WordCounter.Func
    {
    class AutofacConfig
    {
        public AutofacConfig(string functionName)
        {
            DependencyInjection.Initialize(builder => 
            {
                builder.RegisterType<UrlWordFrequencyAnalyser>().As<IUrlWordFrequencyAnalyser>();
                builder.RegisterType<WordParser>().As<IWordParser>();
                builder.RegisterType<UrlRequestor>().As<IUrlRequestor>();
            }, functionName);
        }
    }
}
