using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Octopus.WordCounter.Func
{
    public interface IUrlRequestor
    {
        Task<string> RequestAsync(Uri url);
    }

    public class UrlRequestor : IUrlRequestor
    {
        public async Task<string> RequestAsync(Uri uri)
        {
            var client = new WebClient();
            var response = await client.DownloadStringTaskAsync(uri);            

            return response;
        }
    }
}