using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using AzureFunctions.Autofac;
using Autofac;
using Octopus.WordCounter.Func;
using System.Collections.Generic;
using System.Linq;

namespace Octopus.WordCounter.Func
{
    [DependencyInjectionConfig(typeof(AutofacConfig))]
    public static class WordCounterFunc
    {
        [FunctionName("WordCounter")]
        public static IActionResult Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = null)] HttpRequest req,
            ILogger log,
            [Inject]Octopus.WordCounter.Func.IUrlWordFrequencyAnalyser wordFrequencyCounter)
        {
            log.LogInformation("Started WordCounterFunc");

            var uri = new Uri(req.Query["url"]);

            Dictionary<string, int> words = wordFrequencyCounter.Count(uri);

            // string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            // dynamic data = JsonConvert.DeserializeObject(requestBody);
            // name = name ?? data?.name;

            return words.Count > 0
                ? (ActionResult)new OkObjectResult($"Hello, {words.Keys.FirstOrDefault()}")
                : new BadRequestObjectResult("Please pass a valid URL on the query string e.g. ?url=HTTP://google.com/");
        }
    }
}
