
using System.Collections.Generic;
using HtmlAgilityPack;

namespace Octopus.WordCounter.Func
{
    public interface IHtmlTextExtractor
    {
        string Extract(string htmlString);
    }
    public class HtmlTextExtractor : IHtmlTextExtractor
    {
        public string Extract(string htmlString)
        {
            var chunks = new List<string>();

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(htmlString);

            foreach (var node in doc.DocumentNode.DescendantsAndSelf())
            {
                if ((node.NodeType == HtmlNodeType.Text))
                {
                    if (isText(node))
                    {
                        chunks.Add(node.InnerText.Trim());
                    }
                }
            }
            return string.Join(" ", chunks);        
        }

        private static bool isText(HtmlNode node)
        {
            if (node.InnerText.Trim() != "" && node.ParentNode.Name != "script" && node.ParentNode.Name != "style")
                return true;
            else
                return false;
        }
    }

}