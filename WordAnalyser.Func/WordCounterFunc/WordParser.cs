using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using AzureFunctions.Autofac;
using HtmlAgilityPack;

namespace Octopus.WordCounter.Func
{
    public interface IWordParser
    {
        IEnumerable<string> Parse(string text);
    }

    public class WordParser: IWordParser
    {
        public IEnumerable<string> Parse(string text)
        {
            var stopWords = new List<char>{',','.',' '};
            var word = string.Empty;
            foreach (char c in text)
            {
                if (stopWords.Contains(c))
                {
                    yield return word;
                    word = string.Empty;
                    continue;
                }
                word += c;
            }
            if (word != string.Empty) yield return word;
        }
    }
} 