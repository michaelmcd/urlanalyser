using System;
using System.Collections.Generic;
using AzureFunctions.Autofac;

namespace Octopus.WordCounter.Func
{
    public interface IUrlWordFrequencyAnalyser
    {
        Dictionary<string, int> Count(Uri uri);
    }

    public class UrlWordFrequencyAnalyser : IUrlWordFrequencyAnalyser
    {
        private readonly IUrlRequestor url;
        private readonly IWordParser wordParser;

        public UrlWordFrequencyAnalyser(
            [Inject]IUrlRequestor url,
            [Inject]IWordParser wordParser)
        {
            this.url = url;
            this.wordParser = wordParser;
        }

        public Dictionary<string, int> Count(Uri uri)
        {
            var source = url.RequestAsync(uri).Result;

            IEnumerable<string> words = wordParser.Parse(source);

            // todo
            return new Dictionary<string, int>(){};
        }
    }
}
