provider "azurerm" {}

resource "azurerm_resource_group" "octopus" {
  name     = "octopus"
  location = "${var.az_location}"
}

resource "random_id" "server" {
  keepers = {
    # Generate a new id each time we switch to a new Azure Resource Group
    rg_id = "${azurerm_resource_group.octopus.name}"
  }

  byte_length = 8
}

resource "azurerm_storage_account" "octopus" {
  name                     = "${random_id.server.hex}"
  resource_group_name      = "${azurerm_resource_group.octopus.name}"
  location                 = "${azurerm_resource_group.octopus.location}"
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_app_service_plan" "octopus" {
  name                = "octopus-service-plan"
  location            = "${azurerm_resource_group.octopus.location}"
  resource_group_name = "${azurerm_resource_group.octopus.name}"
  kind                = "FunctionApp"

  sku {
    tier = "Dynamic"
    size = "Y1"
  }
}

resource "azurerm_function_app" "word_counter" {
  name                      = "WordCounter-${random_id.server.hex}"
  location                  = "${azurerm_resource_group.octopus.location}"
  resource_group_name       = "${azurerm_resource_group.octopus.name}"
  app_service_plan_id       = "${azurerm_app_service_plan.octopus.id}"
  storage_connection_string = "${azurerm_storage_account.octopus.primary_connection_string}"
  version                   = "0.1.0"

  provisioner "local-exec" {
    command = "az functionapp deployment source config-zip -g ${azurerm_resource_group.octopus.name} -n ${azurerm_function_app.word_counter.name} --src ${data.archive_file.word_counter_func_app.output_path}"
  }
}

data "archive_file" "word_counter_func_app"{  
  type        = "zip"
  source_dir  = "${path.module}\\..\\WordAnalyser.Func/"
  output_path = "${path.module}\\..\\word_counter_func_app.zip"
}