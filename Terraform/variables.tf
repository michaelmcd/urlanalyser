variable "g_region" {
  default = "europe-west2"
}

variable "az_location" {
  default = "northeurope"
}

variable "git_enabled" {
  default = true
}

