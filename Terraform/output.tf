output "identity_principal_id" {

  description = "The MSI identity properties set on the function app."

  value       = "${azurerm_function_app.word_counter.identity}"

}

output "host_name" {

  description = "The function apps host name."

  value       = "${azurerm_function_app.word_counter.default_hostname}"

}

output "function_id" {
    value      = "${azurerm_function_app.word_counter.id}"
}