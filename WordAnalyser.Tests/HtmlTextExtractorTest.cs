
using System;
using System.IO;
using Xunit;
using Moq;
using System.Threading.Tasks;
using Octopus.WordCounter.Func;

namespace Octopus.UrlAnalyser.Tests
{
    public class HtmlTextExtractorTest
    {
        HtmlTextExtractor htmlTextExtractor = new HtmlTextExtractor();

        [Theory]
        [InlineData("<head><body>My name is Michael</body></head>", "My name is Michael")]
        public void ShouldExtractTextFromSimpleHTMLMarkup(string htmlString, string expectedHtmlText)
        {
            var htmlText = htmlTextExtractor.Extract(htmlString);

            Assert.Equal(expectedHtmlText, htmlText);
        } 

        [Fact]
        public void ShouldExtractTextFromComplexHTMLMarkup()
        {
            var htmlString = File.ReadAllText("google.html");
            var expectedHtmlText = File.ReadAllText("google.txt");
            var htmlText = htmlTextExtractor.Extract(htmlString);

            Assert.Equal(expectedHtmlText, htmlText);
        } 
    }
}

