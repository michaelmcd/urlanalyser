using System;
using Xunit;
using Octopus.WordCounter.Func;
using Moq;
using System.Threading.Tasks;
using System.Linq;
using System.IO;

namespace Octopus.UrlAnalyser.Tests
{
    public class WordExtractorTest
    {
        HtmlTextExtractor htmlTextExtractor = new HtmlTextExtractor();

        [Theory]
        [InlineData("My name is Michael", 4)]
        [InlineData("My name is Michael.", 4)]
        public void ShouldReturCorrectWordCount(string htmlString, int wordCount)
        {
            var words = new WordParser().Parse(htmlString).Count<string>();

            Assert.Equal(wordCount, words);
        } 
        
        [Theory]
        [InlineData("My name is Michael")]
        [InlineData("My name is Michael.")]
        public void ShouldReturWordsInString(string htmlString)
        {
            var words = new WordParser().Parse(htmlString);

            Assert.Equal("My", words.ElementAt(0));
            Assert.Equal("name", words.ElementAt(1));
            Assert.Equal("is", words.ElementAt(2));
            Assert.Equal("Michael", words.ElementAt(3));
        } 

        [Fact]
        public void CanCountWordsInTextFile()
        {            
            var text = File.ReadAllText("google.txt");

            var words = new WordParser().Parse(text);

            Assert.Equal(49, words.Count());
        }
    }
}